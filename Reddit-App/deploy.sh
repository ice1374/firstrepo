#!/bin/bash

git clone https://github.com/Artemmkin/reddit.git
source /usr/local/rvm/scripts/rvm
cd reddit && bundle install
puma -d